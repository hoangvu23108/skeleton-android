package com.kenk11.skeleton.ui.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.kenk11.skeleton.R
import com.kenk11.skeleton.utils.UserStore


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
            nextScreen()
        }, 2000)
    }

    private fun nextScreen() {
        if (UserStore.isFirstTime()) {
            UserStore.launchedFirstTime()
            setupFirstTime()
        }
        val intent = Intent(this@SplashActivity, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun setupFirstTime() {
        TODO("setup for first time launched")
    }
}