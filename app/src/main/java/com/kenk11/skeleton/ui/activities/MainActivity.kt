package com.kenk11.skeleton.ui.activities

import android.os.Bundle
import com.kenk11.skeleton.R

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupDrawer()
    }

    private fun setupDrawer() {
    }

}
