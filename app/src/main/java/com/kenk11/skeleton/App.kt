package com.kenk11.skeleton

import android.app.Application
import com.kenk11.skeleton.constants.Constants
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

/**
 * Created by ken on 8/25/17.
 */
class App : Application() {

    companion object {
        lateinit var instance: App
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        setupCustomFont()
    }

    private fun setupCustomFont() {
        val config = CalligraphyConfig.Builder()
                .setDefaultFontPath(Constants.DEFAULT_FONT_PATH)
                .setFontAttrId(R.attr.fontPath)
                .build()
        CalligraphyConfig.initDefault(config)
    }
}