package com.kenk11.skeleton.constants

/**
 * Created by ken on 8/28/17.
 */
object Constants {
    val DEFAULT_FONT_PATH = "fonts/Times New Roman Bold.ttf"
}