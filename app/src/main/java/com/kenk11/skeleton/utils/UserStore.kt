package com.kenk11.skeleton.utils

import android.content.Context
import android.content.SharedPreferences
import com.kenk11.skeleton.App


/**
 * Created by Ken Lâm on 8/28/17.
 */
object UserStore {
    val APP_PREFERENCE = "Global App Preference"
    val KEY_FIRST_TIME = "first time"
    val NOTIFICATION_ID = "notification id"


    // *** Data manipulation helper ***

    fun getAppPreference(): SharedPreferences {
        return App.instance.getSharedPreferences(APP_PREFERENCE, Context.MODE_PRIVATE)
    }

    fun saveData(key: String, data: String?) {
        val pref = getAppPreference()
        val editor = pref.edit()
        editor.putString(key, data ?: "")
        editor.apply()
    }

    fun saveData(key: String, data: Int) {
        val pref = getAppPreference()
        val editor = pref.edit()
        editor.putInt(key, data)
        editor.apply()
    }

    fun saveData(key: String, data: Boolean) {
        val pref = getAppPreference()
        val editor = pref.edit()
        editor.putBoolean(key, data)
        editor.apply()
    }

    fun clearData() {
        val pref = getAppPreference()
        val editor = pref.edit()
        editor.clear()
        editor.apply()
    }

    // *** Data serializing ***
    fun launchedFirstTime(): Unit {
        saveData(KEY_FIRST_TIME, false)
    }


    // *** Data de-serializing ***
    fun isFirstTime(): Boolean {
        return getAppPreference().getBoolean(KEY_FIRST_TIME, true)
    }


}